COMMERCE SHIPPING FEE
=====================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration

INTRODUCTION
------------

Provides refund functionality for Drupal Commerce.

为Drupal Commerce提供退款功能。

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

按照您通常安装已贡献的Drupal模块的方式进行安装。
请参阅：https://www.drupal.org/documentation/install/modules-themes/modules-8
以获取更多信息。

CONFIGURATION
-------------

* Configure order-types
  Configure existing order-types or new order-types in "/admin/commerce/config/order-types".
  Select "Commerce refunds order" in "Workflow".
* After the user successfully places an order, the administrator enters the corresponding order interface to confirm the payment.
  The buyer will see a refund request link in the "Order Operation" column of the order list interface ("/user/*/orders").
* Refund request conditions: 1. Payment completed and not shipped. 2. Ship and confirm receipt by the user.
* 配置订单类型
  在“/admin/commerce/config/order-types”中配置现有的订单类型或新订单类型。
  在“Workflow”中选择“Commerce refunds order”。
* 用户下单成功后，管理员进入相应的订单界面，确认付款。
  买家将在订单列表界面的“订单操作”列中看到退款请求链接（“/user/*/orders”）。
* 退款申请条件：1。付款已完成但未发货。2.发货并由买家用户确认收货。
