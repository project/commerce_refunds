<?php

namespace Drupal\commerce_refunds\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReturnInfoForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_refund_record storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commerceRefundsStorage;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The commerce_order entity.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected $order;

  /**
   * The commerce_refund_record entity.
   *
   * @var \Drupal\commerce_refunds\Entity\RefundRecord
   */
  protected $refundRecord = NULL;

  /**
   * The current route match.
   *
   */
  protected $isCustomer = FALSE;

  /**
   * @var \Drupal\commerce_shipping\ShippingMethodStorage
   */
  protected $shippingMethodStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')
        ->getStorage('commerce_refund_record'),
      $container->get('current_route_match'),
    );
  }

  /**
   * Creates a MyForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $commerce_refunds_storage
   *   The commerce_payment storage handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityStorageInterface     $commerce_refunds_storage,
    RouteMatchInterface        $route_match
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->commerceRefundsStorage = $commerce_refunds_storage;
    $this->routeMatch = $route_match;
    $this->order = $this->routeMatch->getParameter('commerce_order');
    $query = $this->commerceRefundsStorage->getQuery();
    $query->condition('order_id', $this->order->id());
    $query->sort('created');
    $refund_record_ids = $query->execute();
    $refund_records = $this->commerceRefundsStorage->loadMultiple($refund_record_ids);
    if (!empty($refund_records)) {
      $this->refundRecord = reset($refund_records);
    }
    $this->shippingMethodStorage = $entity_type_manager->getStorage('commerce_shipping_method');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_refunds_return_info_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $order = $this->routeMatch->getParameter('commerce_order');
    $customer_id = $order->getCustomerId();
    $current_uid = \Drupal::currentUser()->id();
    if ($customer_id == $current_uid) {
      $this->isCustomer = TRUE;
    }

    $return_remark = $this->refundRecord->get('return_remark')->value;
    if (\Drupal::currentUser()
      ->hasPermission('administer commerce_refund_record entities')) {
      $form['remarks'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Return Remarks'),
        '#format' => 'full_html',
        '#default_value' => $return_remark,
      ];
    }
    if ($this->isCustomer) {
      $form['remarks_info'] = [
        '#type' => 'item',
        '#title' => $this->t('Return Remarks'),
        '#markup' => $return_remark,
      ];

      $shipping_methods = $this->shippingMethodStorage->loadMultiple();
      $shipping_methods_options = [];
      foreach ($shipping_methods as $shipping_method) {
        $shipping_method_name = $shipping_method->getName();
        $shipping_method_id = $shipping_method->id();
        $shipping_methods_options[$shipping_method_id] = $shipping_method_name;
      }
      $form['shipping_methods'] = [
        '#type' => 'select',
        '#title' => t("Select Shipping Method"),
        '#options' => $shipping_methods_options,
        '#default_value' => $this->refundRecord->get('shipping_method')->target_id,
      ];

      $form['tracking_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Tracking Code'),
        '#description' => $this->t('Please enter your logistics tracking code of return.'),
        '#default_value' => $this->refundRecord->get('return_tracking_code')->value,
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order_id = $this->order->id();
    $shipping_method = $form_state->getValue('shipping_methods');
    $tracking_code = $form_state->getValue('tracking_code');
    if (!empty($shipping_method) && !empty($tracking_code)) {
      $this->refundRecord->set('shipping_method', $shipping_method);
      $this->refundRecord->set('return_tracking_code', $tracking_code);
      $this->refundRecord->save();
      $order = Order::load($order_id);
      if ($order->getState()->getId() != "returned") {
        \Drupal::service('commerce_refunds.refunds_services')
          ->confirmReturned($this->order);
      }
    }
    $remarks = $form_state->getValue('remarks');
    if (!empty($remarks)) {
      $this->refundRecord->set('return_remark', $remarks);
    }
    $this->refundRecord->save();

    if (\Drupal::currentUser()
      ->hasPermission('administer commerce_refund_record entities')) {
      $form_state->setRedirect('entity.commerce_order.canonical', ['commerce_order' => $order_id]);
    }
    else {
      $commerce_user_orders_views = Views::getView('commerce_user_orders');
      if ($commerce_user_orders_views) {
        $order_page = $commerce_user_orders_views->getDisplay('order_page');
        $order_page_url = $order_page->getUrl();
        $order_page_url->setRouteParameter('user', \Drupal::currentUser()
          ->id());
        $form_state->setRedirectUrl($order_page_url);
      }
    }
  }

}
