<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the list builder handler for the Refund record entity.
 */
class RefundRecordListBuilder extends EntityListBuilder {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $refundRecordStorage;

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new PaymentListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityStorageInterface $refund_record_storage
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RouteMatchInterface $route_match, EntityStorageInterface $refund_record_storage) {
    parent::__construct($entity_type, $storage);
    $this->routeMatch = $route_match;
    $this->refundRecordStorage = $refund_record_storage;
    $this->order = $route_match->getParameter('commerce_order');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
        ->getStorage('commerce_refund_record'),
    );
  }

  protected function getEntityIds() {
    if (empty($this->order)) {
      $query = $this->getStorage()->getQuery()
        ->accessCheck(TRUE)
        ->sort($this->entityType->getKey('id'));
    }
    else {
      $query = $this->getStorage()->getQuery()
        ->condition('order_id', $this->order->id())
        ->accessCheck(TRUE)
        ->sort($this->entityType->getKey('id'));
    }
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['if_return'] = $this->t('If return');
    $header['return_tracking'] = $this->t('Tracking');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $url = $entity->toUrl();
    $option = $url->getOption('query');
    $option['order_id'] = $entity->get('order_id')->target_id;
    $url->setOption('query', $option);
    $row['label']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $url,
    ];
    $if_return = $this->t("No");
    if ($entity->get('if_return')->value) {
      $edit = Link::fromTextAndUrl($this->t('Edit refund remarks'), Url::fromRoute('commerce_refunds.return_tracking_code_form', [
        'user' => $entity->get('uid')->target_id,
        'commerce_order' => $entity->get('order_id')->target_id,
      ], [
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 700,
            'title' => $this->t('Input return remarks'),
          ]),
        ],
      ]))->toString();
      $if_return = $this->t("Yes @edit", [
        "@edit" => $edit,
      ]);
    }
    $row['if_return']['data'] = [
      '#type' => 'markup',
      '#markup' => $if_return,
    ];
    $row['return_tracking']['data'] = [
      '#type' => 'link',
      '#title' => $this->t("Tracking return"),
      '#url' => Url::fromRoute('commerce_refunds.tracking_return', [
        'commerce_order' => $entity->get('order_id')->target_id,
        'commerce_refund_record' => $entity->id(),
      ], []),
      '#attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];
    return $row + parent::buildRow($entity);
  }

}
