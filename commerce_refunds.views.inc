<?php

function commerce_refunds_views_data_alter(array &$data) {
  /**
   * 显示给客户的订单操作：确认收货，退款，退货
   * Order operations displayed to customers: confirm receipt, refund, return
   */
  $data['commerce_order_item']['commerce_refunds'] = [
    'title' => t('Order operation'),
    'field' => [
      'title' => t('Order operation'),
      'help' => t('Order operation for order views'),
      'id' => 'commerce_refunds_order_operation',
    ],
  ];
}
