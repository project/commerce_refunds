<?php

namespace Drupal\commerce_refunds\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RefundRejectedReasonForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_refund_record storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commerceRefundsStorage;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The commerce_order entity.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected $order;

  /**
   * The commerce_refund_record entity.
   *
   * @var \Drupal\commerce_refunds\Entity\RefundRecord
   */
  protected $refundRecord;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')
        ->getStorage('commerce_refund_record'),
      $container->get('current_route_match'),
    );
  }

  /**
   * Creates a MyForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $commerce_refunds_storage
   *   The commerce_payment storage handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityStorageInterface     $commerce_refunds_storage,
    RouteMatchInterface        $route_match
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->commerceRefundsStorage = $commerce_refunds_storage;
    $this->routeMatch = $route_match;
    $this->order = $this->routeMatch->getParameter('commerce_order');
    $this->refundRecord = NULL;
    $query = $this->commerceRefundsStorage->getQuery();
    $query->condition('order_id', $this->order->id());
    $query->sort('created');
    $refund_record_ids = $query->execute();
    $refund_records = $this->commerceRefundsStorage->loadMultiple($refund_record_ids);
    if (!empty($refund_records)) {
      $this->refundRecord = reset($refund_records);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_refunds_refund_reject_reason_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $reason = '';
    if (!empty($this->refundRecord)) {
      $reason = $this->refundRecord->get('reject_reason')->value;
    }
    $form['reason'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Reject Reason'),
      '#format' => 'full_html',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order_id = $this->order->id();
    if (!empty($this->refundRecord)) {
      $reject_reason = $form_state->getValue('reason');
      $this->refundRecord->set('reject_reason', $reject_reason);
      $this->refundRecord->save();
    }
    $this->order->getState()->applyTransitionById('reject_refund_request');
    $this->order->save();
    $form_state->setRedirect('entity.commerce_order.canonical', ['commerce_order' => $order_id]);
  }

}
