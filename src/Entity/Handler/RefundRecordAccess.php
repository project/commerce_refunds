<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides the access handler for the Refund record entity.
 */
class RefundRecordAccess extends EntityAccessControlHandler {

  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $user = \Drupal::currentUser();
    if ($user->isAnonymous()) {
      return parent::checkCreateAccess($account, $context, $entity_bundle);
    }
    return AccessResult::allowed();
  }

}
