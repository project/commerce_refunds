<?php

namespace Drupal\commerce_refunds\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RefundRejectForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_payment storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commercePaymentStorage;

  /**
   * The commerce_refund_record storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commerceRefundsStorage;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('commerce_payment'),
      $container->get('entity_type.manager')
        ->getStorage('commerce_refund_record'),
      $container->get('current_route_match'),
    );
  }

  /**
   * Creates a MyForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $commerce_payment_storage
   *   The commerce_payment storage handler.
   * @param \Drupal\Core\Entity\EntityStorageInterface $commerce_refunds_storage
   *   The commerce_payment storage handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityStorageInterface     $commerce_payment_storage,
    EntityStorageInterface     $commerce_refunds_storage,
    RouteMatchInterface        $route_match
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->commercePaymentStorage = $commerce_payment_storage;
    $this->commerceRefundsStorage = $commerce_refunds_storage;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_refunds_refund_reject_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<p>Are you sure you want to reject the user\'s refund application?</p>',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /**@var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->routeMatch->getParameter('commerce_payment');
    $order_id = $payment->getOrderId();
    $refund_records = $this->commerceRefundsStorage->loadByProperties([
      'order_id' => $order_id,
    ]);
    if (!empty($refund_records)) {
      $refund_record = reset($refund_records);
      $refund_record->setState(3);
      $result = $refund_record->save();
      if ($result == SAVED_UPDATED) {
        \Drupal::messenger()
          ->addMessage($this->t("Refund request of user has been rejected"));
      }
    }
    $form_state->setRedirectUrl(Url::fromRoute('entity.commerce_payment.collection', ['commerce_order' => $order_id], []));
  }

}
