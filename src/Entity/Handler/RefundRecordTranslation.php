<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Provides the translation handler for the Refund record entity.
 */
class RefundRecordTranslation extends ContentTranslationHandler {

}
