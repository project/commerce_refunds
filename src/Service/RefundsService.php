<?php

namespace Drupal\commerce_refunds\Service;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Implements Class RefundService.
 */
class RefundsService {

  public function confirmReceipt(OrderInterface $order) {
    $order->getState()->applyTransitionById('confirm_receipt');
    $order->save();
  }

  public function applyRefund(OrderInterface $order) {
    $order->getState()->applyTransitionById('apply_refund');
    $order->save();
  }

  public function confirmReturned(OrderInterface $order) {
    $order->getState()->applyTransitionById('refund_return');
    $order->save();
  }

  public function refund(OrderInterface $order) {
    $order->getState()->applyTransitionById('refund');
    $order->save();
  }

}
