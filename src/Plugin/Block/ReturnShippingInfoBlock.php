<?php

namespace Drupal\commerce_refunds\Plugin\Block;

use Drupal\commerce_logistics\LogisticsQueryPluginManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Block displaying return logistics information
 * 显示退货物流信息的区块
 *
 * @Block(
 *   id = "return_shipment_info_block",
 *   admin_label = @Translation("Return shipping info block"),
 *   category = @Translation("Commerce refunds")
 * )
 */
class ReturnShippingInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\commerce_logistics\LogisticsQueryPluginManager
   */
  protected $logisticsQueryManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The logistics detail storage.
   *
   * @var \Drupal\commerce_shipping\ShipmentStorage
   */
  protected $shipmentStorage;

  /**
   * The logistics detail storage.
   *
   * @var \Drupal\commerce_logistics\Entity\Handler\ShipmentInfoStorage
   */
  protected $shipmentInfoStorage;

  /**
   * The logistics company storage.
   *
   * @var \Drupal\commerce_shipping\ShippingMethodStorage
   */
  protected $shippingMethodStorage;

  /**
   * Constructs a new ShipmentInfoBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_logistics\LogisticsQueryPluginManager $logistics_query_manager
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array                       $configuration,
                                $plugin_id,
                                $plugin_definition,
    LogisticsQueryPluginManager $logistics_query_manager, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logisticsQueryManager = $logistics_query_manager;
    $this->routeMatch = $route_match;
    $this->shipmentStorage = $entity_type_manager->getStorage('commerce_shipment');
    $this->shipmentInfoStorage = $entity_type_manager->getStorage('shipment_info');
    $this->shippingMethodStorage = $entity_type_manager->getStorage('commerce_shipping_method');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.logistics_query'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'logistics_info_block',
      '#attached' => ['library' => ['commerce_logistics/base']],
      '#cache' => [
        'max-age' => 0,
      ],
    ];;
    $commerce_refund_record = $this->routeMatch->getParameter('commerce_refund_record');
    $data = [];
    if(!empty($commerce_refund_record)){
      $mail_number = $commerce_refund_record->get('return_tracking_code')->value;
      $shipping_method_id = $commerce_refund_record->get('shipping_method')->target_id;
      $config = \Drupal::config('commerce_logistics.shipping_info_query');
      $company_code = $config->get("code$shipping_method_id");
      if (empty($mail_number) || empty($company_code)) {
        $build['#data'] = NULL;
        return $build;
      }
      $nodes_logistics_query = $this->logisticsQueryManager->createInstance("aggregation_logistics_query");
      $param = [
        'com' => $company_code,
        'num' => $mail_number,
      ];
      $data = $nodes_logistics_query->shipping_info_data($param);
      $build['#data'] = $data;
    } else {
      $build['#data'] = NULL;
    }
    return $build;
  }

}
