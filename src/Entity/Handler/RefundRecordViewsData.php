<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\views\EntityViewsData;

/**
 * Provides the Views data handler for the Refund record entity.
 */
class RefundRecordViewsData extends EntityViewsData {

}
