<?php

namespace Drupal\commerce_refunds\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Provides the Refund record entity.
 *
 * @ContentEntityType(
 *   id = "commerce_refund_record",
 *   label = @Translation("Refund record"),
 *   label_collection = @Translation("Refund records"),
 *   label_singular = @Translation("refund record"),
 *   label_plural = @Translation("refund records"),
 *   label_count = @PluralTranslation(
 *     singular = "@count refund record",
 *     plural = "@count refund records",
 *   ),
 *   base_table = "commerce_refund_record",
 *   data_table = "commerce_refund_record_field_data",
 *   translatable = "TRUE",
 *   handlers = {
 *     "access" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordAccess",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\commerce_refunds\Form\RefundRecordForm",
 *       "add" = "Drupal\commerce_refunds\Form\RefundRecordAddForm",
 *       "edit" = "Drupal\commerce_refunds\Form\RefundRecordEditForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "storage" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordStorage",
 *     "storage_schema" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordStorageSchema",
 *     "list_builder" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordListBuilder",
 *     "view_builder" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordViewBuilder",
 *     "views_data" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordViewsData",
 *     "translation" = "Drupal\commerce_refunds\Entity\Handler\RefundRecordTranslation",
 *   },
 *   admin_permission = "administer commerce_refund_record entities",
 *   entity_keys = {
 *     "id" = "commerce_refund_record_id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   field_ui_base_route = "entity.commerce_refund_record.collection",
 *   links = {
 *     "add-form" = "/commerce/commerce-refund-record/add",
 *     "canonical" = "/admin/commerce/commerce-refund-record/{commerce_refund_record}",
 *     "collection" = "/admin/commerce/commerce-refund-record",
 *     "delete-form" = "/admin/commerce/commerce-refund-record/{commerce_refund_record}/delete",
 *     "edit-form" = "/admin/commerce/commerce-refund-record/{commerce_refund_record}/edit",
 *   },
 * )
 */
class RefundRecord extends ContentEntityBase implements RefundRecordInterface {

  use EntityChangedTrait;

  use EntityOwnerTrait;

  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  public function getPayments() {
    $order = $this->get('order_id')->entity;
    return \Drupal::entityTypeManager()->getStorage('commerce_payment')->loadMultipleByOrder($order);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Title"))
      ->setRequired(TRUE)
      ->setSetting("max_length", 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Please briefly explain the reasons for requesting a refund. After receiving your refund application, the merchant will process it according to your request.'))
      ->setRequired(FALSE)
      ->setSetting("max_length", 500)
      ->setDisplayOptions("form", [
        'type' => "string_textfield",
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE)
      ->setDisplayConfigurable("form", TRUE);

    $fields['reject_reason'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Reject Reason'))
      ->setDescription(t('The reason why the merchant refused the refund is displayed here.'))
      ->setRequired(FALSE)
      ->setSetting("max_length", 500)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['refund_amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Refund amount'))
      ->setDescription(t('The amount you want to refund.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['if_return'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('If need return?'))
      ->setDescription(t('Whether to return goods before refund.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shipping_method'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("Shipping method"))
      ->setDescription(t("Shipping method used for refunds return."))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'commerce_shipping_method')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['return_tracking_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Tracking Code"))
      ->setDescription(t('Enter the tracking code for the buyer\'s return.'))
      ->setRequired(FALSE)
      ->setSetting("max_length", 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['return_remark'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Return Remarks'))
      ->setDescription(t('The reason why the merchant refused the refund is displayed here.'))
      ->setRequired(FALSE)
      ->setSetting("max_length", 500)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['feedback_files'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Upload pictures or videos to feedback on order issues.'))
      ->setRevisionable(TRUE)
      ->setCardinality(5)
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'commerce_refunds',
        'file_extensions' => 'png jpg jpeg mp4',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'file',
      ))
      ->setDisplayOptions('form', array(
        'type' => 'file_default',
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("Order"))
      ->setDescription(t("Orders to be refunded."))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['order_item_ids'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("Order Item"))
      ->setDescription(t("Order Items to be refunded."))
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'commerce_order_item')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("User"))
      ->setDescription(t("User applying for refund."))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable("view", TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t("Created"))
      ->setDescription(t("The time that the entity was created."));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t("Changed"))
      ->setDescription(t("The time that the entity was last edited."));

    return $fields;
  }

}
