<?php

namespace Drupal\commerce_refunds\Plugin\views\field;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * TODO: class docs.
 *
 * @ViewsField("commerce_refunds_order_operation")
 */
class OrderOperation extends FieldPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_shipment storage handler.
   *
   * @var \Drupal\commerce_shipping\ShipmentStorage
   */
  protected $shipmentStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The commerce_refund_record storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $refundRecordStorage;

  /**
   * The commerce_payment storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('commerce_shipment'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
        ->getStorage('commerce_refund_record'),
      $container->get('entity_type.manager')->getStorage('commerce_payment')
    );
  }

  /**
   * Creates a RefundApplication instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $shipment_storage
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $refund_record_storage
   *   The commerce_refund_record storage handler.
   * @param \Drupal\Core\Entity\EntityStorageInterface $payment_storage
   *   The commerce_payment storage handler.
   */
  public function __construct(
    array                      $configuration,
                               $plugin_id,
                               $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityStorageInterface     $shipment_storage,
    AccountInterface           $current_user,
    EntityStorageInterface     $refund_record_storage,
    EntityStorageInterface     $payment_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->shipmentStorage = $shipment_storage;
    $this->currentUser = $current_user;
    $this->refundRecordStorage = $refund_record_storage;
    $this->paymentStorage = $payment_storage;
  }

  /**
   * @{inheritdoc}
   */
  public function query() {
  }

  /**
   * Define the available options
   *
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_empty'] = ['default' => TRUE];
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $index = $values->index;
    if ($index != 0) {
      return NULL;
    }
    $order_item = $this->getEntity($values);
    $order = $order_item->getOrder();
    $state = $order->getState()->getId();
    $paid_amount = $order->getTotalPaid()->getNumber();
    $result = NULL;
    if ($paid_amount >= 0) {
      switch ($state) {
        case "ordered":
        case "shipped":
        case "received":
        case "refund_requested":
        case "refund_agreed":
        case "refunded":
        case "returned":
        case "return_received":
        case "refund_request_rejected":
          $result = $this->getRefundLinkOrStateText($order->id());
          break;
      }
    }
    return $result;

  }

  public function getRefundLinkOrStateText($order_id) {
    $build = [
      '#type' => 'markup',
      '#markup' => '',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $refunds = $this->refundRecordStorage->loadByProperties([
      'order_id' => $order_id,
    ]);

    $user_id = \Drupal::currentUser()->id();
    $refund_request_url = Url::fromRoute('entity.commerce_refund_record.add_form', [], [
      'query' => [
        'order_id' => $order_id,
        'destination' => "/user/$user_id/orders",
      ],
      'attributes' => [
        'target' => 'blank',
      ],
    ]);
    $confirm_receipt_url = Url::fromRoute('commerce_refunds.confirm_receipt', [
      'user' => $user_id,
      'commerce_order' => $order_id,
    ], []);
    $confirm_return_url = Url::fromRoute('commerce_refunds.return_tracking_code_form', [
      'user' => $user_id,
      'commerce_order' => $order_id,
    ], []);
    $order = Order::load($order_id);
    $state = $order->getState()->getId();
    if (empty($refunds)) {
      switch ($state) {
        case "shipped":
          $build['#markup'] = Link::fromTextAndUrl($this->t("Confirm receipt"), $confirm_receipt_url)
            ->toString();
          return $build;
        default:
          $payments = $this->paymentStorage->loadMultipleByOrder($order);
          $payment = reset($payments);
          $payment_state_id = $payment->getState()->getId();
          if ($payment_state_id === "completed") {
            $build['#markup'] = Link::fromTextAndUrl($this->t("Request a refund"), $refund_request_url)
              ->toString();
          }
          return $build;
      }
    }
    else {
      $refund = reset($refunds);
      $if_return = $refund->get('if_return')->value;
      $refund_amount = $refund->get('refund_amount')
        ->first()
        ->toPrice()
        ->getNumber();
      $currency_code = $refund->get('refund_amount')
        ->first()
        ->toPrice()
        ->getCurrencyCode();
      if ($state == "refund_agreed" && $if_return) {
        $remakrs = $refund->get('return_remark')->value;
        $refunded_link = Link::fromTextAndUrl($this->t("Return Tracking Code"), $confirm_return_url)
          ->toString();
        $build['#markup'] = "<p>{$remakrs}</p>" . $refunded_link;
        return $build;
      }
      switch ($state) {
        case "refund_requested":
          $build['#markup'] = $this->t('Refund Pending');
          return $build;
        case "refund_agreed":
          $build['#markup'] = $this->t('Refund agreed.');
          return $build;
        case "refunded":
          $build['#markup'] = $this->t('Refunded: %number %currency_code', [
            '%number' => number_format($refund_amount, 2),
            '%currency_code' => $currency_code,
          ]);
          return $build;
        case "returned":
          $refund_records = $this->refundRecordStorage->loadByProperties([
            'order_id' => $order_id,
          ]);
          $refund_record = reset($refund_records);
          $tracking_link = Link::fromTextAndUrl($this->t('Tracking return'), Url::fromRoute('commerce_refunds.tracking_return', [
            'commerce_order' => $order_id,
            'commerce_refund_record' => $refund_record->id(),
          ], [
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => 700,
              ]),
            ],
          ]))->toString();
          $edit_link = Link::fromTextAndUrl($this->t('Edit return shipment info'), Url::fromRoute('commerce_refunds.return_tracking_code_form', [
            'user' => $refund_record->get('uid')->target_id,
            'commerce_order' => $order_id,
          ], [
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => 700,
              ]),
            ],
          ]))->toString();
          $build['#markup'] = $this->t("Returned( @tracking_link ),waiting for merchant to confirm receipt.@edit", [
            '@tracking_link' => $tracking_link,
            '@edit' => $edit_link,
          ]);
          return $build;
        case "return_received":
          $build['#markup'] = $this->t('The return has been received, waiting for the merchant to process the refund.');
          return $build;
        case "refund_request_rejected":
          $reject_title = $this->t('Refund request rejected,');
          $reject_reason = $refund->get('reject_reason')->value;
          $reject_html = "<p>{$reject_title}</p><p>{$reject_reason}</p>";
          $build['#markup'] = $reject_html . Link::fromTextAndUrl($this->t("Request a refund again"), $refund_request_url)
              ->toString();
          return $build;
      }
    }
    return $build;
  }

}
