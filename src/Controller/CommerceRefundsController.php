<?php
/**
 */

namespace Drupal\commerce_refunds\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Laminas\Diactoros\Response\RedirectResponse;

class CommerceRefundsController extends ControllerBase {

  /**
   * Confirm receipt
   * (the customer can only proceed with the shipment operation after the administrator receives the order payment)
   * 确认收货（订单款项收到，管理员进行发货操作后，顾客才能进行该操作）
   */
  public function confirmReceipt(UserInterface $user, OrderInterface $commerce_order) {
    \Drupal::service('commerce_refunds.refunds_services')->confirmReceipt($commerce_order);
    return new RedirectResponse(Url::fromRoute('entity.commerce_order.user_view', [
      'user' => $user->id(),
      'commerce_order' => $commerce_order->id(),
    ], [])->toString());
  }

  /**
   * Confirmation of receipt in the refund and return process
   * (it is necessary for the customer to confirm the return before refunding).
   * 退款退货流程中的确认收货操作（需要先由顾客确认退货后才能进行退款）。
   */
  public function confirmReturn(UserInterface $user, OrderInterface $commerce_order) {
    \Drupal::service('commerce_refunds.refunds_services')->confirmReturned($commerce_order);
    return new RedirectResponse(Url::fromRoute('entity.commerce_order.user_view', [
      'user' => $user->id(),
      'commerce_order' => $commerce_order->id(),
    ], [])->toString());
  }

  public function trackingReturn(){
    $customblock = \Drupal::service('plugin.manager.block')->createInstance('return_shipment_info_block', []);
    return $customblock->build();
  }

}
