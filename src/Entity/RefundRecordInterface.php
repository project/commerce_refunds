<?php

namespace Drupal\commerce_refunds\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Interface for Refund record entities.
 */
interface RefundRecordInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

}
