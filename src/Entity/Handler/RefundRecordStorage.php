<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Provides the storage handler for the Refund record entity.
 */
class RefundRecordStorage extends SqlContentEntityStorage {

}
