'use strict';
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.refresh_on_modal_close = {
    attach: function (context, settings) {
      $('#drupal-modal').on('dialogclose', function (event) {
        let views = settings.views.ajaxViews;
        let dom_id = views[Object.keys(views)[0]]['view_dom_id'];
        let selector = '.js-view-dom-id-' + dom_id;
        $(selector).triggerHandler('RefreshView');
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
