<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides the view builder handler for the Refund record entity.
 */
class RefundRecordViewBuilder extends EntityViewBuilder {

}
