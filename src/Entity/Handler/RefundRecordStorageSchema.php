<?php

namespace Drupal\commerce_refunds\Entity\Handler;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Provides the storage schema handler for the Refund record entity.
 */
class RefundRecordStorageSchema extends SqlContentEntityStorageSchema {

}
