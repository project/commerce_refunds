<?php

namespace Drupal\commerce_refunds\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides the default form handler for the Refund record entity.
 */
class RefundRecordForm extends ContentEntityForm {


  protected $order;


  /**
   * Constructs a RefundRecordForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $order_id = \Drupal::request()->query->get('order_id');
    $this->order = Order::load($order_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  protected function prepareEntity() {
    parent::prepareEntity();
    if (empty($this->order)) {
      throw new NotFoundHttpException();
    }
    $refunds = \Drupal::entityTypeManager()
      ->getStorage('commerce_refund_record')
      ->loadByProperties([
        'order_id' => $this->order->id(),
      ]);
    if (!empty($refunds)) {
      $this->entity = reset($refunds);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $total_paid = 0;
    $refund_method = 0;
    if (empty($this->order) && !$this->entity->get('order_id')->isEmpty()) {
      $this->order = $this->entity->get('order_id')->entity;
    }
    if (!empty($this->order) && !$this->entity->get('refund_amount')
        ->isEmpty()) {
      $refund_amount = $this->entity->get('refund_amount')
        ->first()
        ->toPrice()
        ->getNumber();
      $order_item_ids = $this->entity->get('order_item_ids')->getValue();
      if (count($order_item_ids) > 0) {
        foreach ($order_item_ids as $order_item_id) {
          if ($order_item_id['target_id'] != 0) {
            $order_item = OrderItem::load($order_item_id['target_id']);
          }
          $total_paid = floatval($order_item->getTotalPrice()
              ->getNumber()) + $total_paid;
        }
      }
      if ($total_paid > ($refund_amount + 0)) {
        $refund_method = 1;
      }
    }
    $form['refund_method'] = [
      '#type' => 'radios',
      '#title' => t('Refund method'),
      '#options' => [
        0 => t('Refund Fully'),
        1 => t('Refund Partially'),
      ],
      '#default_value' => $refund_method,
      '#weight' => 0,
    ];
    if (empty($this->order)) {
      $this->order = $this->entity->get('order_id')->entity;
    }
    $refund_amount = 0;
    if ($this->entity->get('refund_amount')->first()) {
      $refund_amount = $this->entity->get('refund_amount')
        ->first()
        ->toPrice()
        ->getNumber();
    }

    $form['enable_refund_amount_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable input of refund amount'),
      '#default_value' => FALSE,
      '#weight' => 0,
      '#states' => [
        'visible' => [
          ':input[name="refund_method"]' => [
            ['value' => 1],
          ],
        ],
      ],
    ];
    $form['refund_amount_number'] = [
      '#type' => 'number',
      '#max' => $this->order->getTotalPaid()->getNumber(),
      '#min' => 0,
      '#default_value' => number_format($refund_amount, 2),
      '#title' => $this->order->getTotalPaid()->getCurrencyCode(),
      '#weight' => 0,
      '#attributes' => [
        'style' => 'width:120px',
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_refund_amount_number"]' => [
            ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $order_id = \Drupal::request()->query->get('order_id');
    $order = Order::load($order_id);
    $order_items = $order->getItems();
    foreach ($order_items as $order_item) {
      $quantity = intval($order_item->getQuantity());
      $options[$order_item->id()] = $this->t('@label, Quantity:@quantity', [
        '@label' => $order_item->label(),
        '@quantity' => $quantity,
      ]);
    }
    $refund_order_items = [];
    if (!$this->entity->get('order_item_ids')->isEmpty()) {
      $order_item_ids = $this->entity->get('order_item_ids')->getValue();
      foreach ($order_item_ids as $order_item_id) {
        $refund_order_items[] = $order_item_id['target_id'];
      }
    }
    else {
      foreach ($order_items as $order_item) {
        $refund_order_items[] = $order_item->id();
      }
    }
    $form['refund_order_items'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Order items'),
      '#description' => $this->t("Select the order items to be refunded."),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $refund_order_items,
      '#states' => [
        'visible' => [
          ':input[name="refund_method"]' => [
            ['value' => 1],
          ],
        ],
      ],
    ];

    $form['if_return']['#weight'] = 2;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $total_paid = 0;
    $refund_method = $form_state->getValue('refund_method');
    $refund_order_item_ids = $form_state->getValue('refund_order_items');
    if ($refund_method == 0) {
      $total_paid = $this->order->getTotalPaid()->getNumber();
    }
    else {
      foreach ($refund_order_item_ids as $refund_order_item_id) {
        if ($refund_order_item_id != 0) {
          $order_item = OrderItem::load($refund_order_item_id);
          $order_item_price = $order_item->getTotalPrice()->getNumber();
          $total_paid = floatval($order_item_price) + $total_paid;
        }
      }
      $enable_refund_amount_number = $form_state->getValue('enable_refund_amount_number');
      $refund_amount_number = $form_state->getValue(['refund_amount_number']);
      if ($enable_refund_amount_number && $refund_amount_number <= 0) {
        $refund_amount_number_error = $this->t('Please enter the refund amount (amount must be greater than zero).');
        $form_state->setErrorByName('refund_amount_number', $refund_amount_number_error);
      }
      if ($enable_refund_amount_number && $total_paid > 0 && $refund_amount_number > $total_paid) {
        $refund_amount_number_error = $this->t('The refund amount cannot be greater than @total_paid.', [
          '@total_paid' => $total_paid,
        ]);
        $form_state->setErrorByName('refund_amount_number', $refund_amount_number_error);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    if (!empty($this->order)) {
      $order_num = $this->order->getOrderNumber();
      $uid = $this->order->get('uid')->target_id;
      $entity->set('order_id', $this->order->id());
      $entity->set('uid', $uid);
      $refund_title = $this->t("Refund request: User ID - @uid, Order number - @order_num", [
        "@uid" => $uid,
        "@order_num" => $order_num,
      ]);
      $entity->set('title', $refund_title);
      $refund_method = $form_state->getValue('refund_method');
      if ($refund_method == 0) {
        /**
         * Full refund
         * 全额退款
         */
        //Calculate the total price of all order items and pass the order item id to the order_item_ids field of the commerce_refund_record entity.
        //计算所有订单项价格总额,并将订单项id传给commerce_refund_record实体的order_item_ids字段。
        $total_paid = 0;
        $order_items = $this->order->getItems();
        $order_item_ids = [];
        foreach ($order_items as $order_item) {
          $total_paid = floatval($order_item->getTotalPrice()
              ->getNumber()) + $total_paid;
          $order_item_ids[] = $order_item->id();
        }
        $entity->set('order_item_ids', $order_item_ids);
        $price = new Price($total_paid, $this->order->getTotalPaid()
          ->getCurrencyCode());
      }
      else {
        /**
         * Partial refund
         * 部分退款
         */
        $refund_amount_number = 0;
        $enable_refund_amount_number = $form_state->getValue('enable_refund_amount_number');
        $refund_order_items = $form_state->getValue('refund_order_items');
        if ($enable_refund_amount_number) {
          // Refund amount in the case of entering a refund amount
          // 输入退款金额情况下的退款金额
          $refund_amount_number = $form_state->getValue('refund_amount_number');
        }
        else {
          // Refund amount without input of refund amount
          // 没输入退款金额情况下的退款金额
          $refund_order_item_ids = $form_state->getValue('refund_order_items');
          foreach ($refund_order_item_ids as $refund_order_item_id) {
            if ($refund_order_item_id != 0) {
              $order_item = OrderItem::load($refund_order_item_id);
              $refund_amount_number = floatval($order_item->getTotalPrice()
                  ->getNumber()) + $refund_amount_number;
            }
          }
        }
        $entity->set('order_item_ids', $refund_order_items);
        $price = new Price($refund_amount_number, $this->order->getTotalPaid()
          ->getCurrencyCode());
      }
      $this->entity->set('refund_amount', $price);
      if ($this->order->getState()->getId() != "refund_requested") {
        \Drupal::service('commerce_refunds.refunds_services')
          ->applyRefund($this->order);
      }
    }
    $saved = parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));
    return $saved;
  }

}
